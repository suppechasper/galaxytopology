### Galaxy Persistent Homology ###

library( shiny )
library( misc3d )

load("data/xyz-reduced-scaled.Rdata")


# 
function(input,output,session){

  data <- reactiveValues()
  data$density <- NULL

  # 3D viewer
  output$galaxies <- renderWebGL({
    if( !is.null(data$density) ){
      p <- data$density
      contour3d(p$d, x=p$x, y=p$y, z=p$z, level=input$isolevel, color="gold", color2="gray")
      print("rendering at level: ")
      print( input$isolevel ) 
    }
  })
 

  o1 <- observeEvent( input$gridsize, {
    data$density <<- kde3d(X[,1], X[,2], X[,3], n = input$gridsize, h = input$bw)
  })
  
  o2 <- observeEvent( input$bw, {
    data$density <<- kde3d(X[,1], X[,2], X[,3], n = input$gridsize, h = input$bw)
  })

  o3 <- observe({
    if( !is.null( data$density) )
    m <- max( data$density$d )
    updateSliderInput(session, "isolevel", min = 0, max = round(m, 2) )
  })


}




