# Topology of the Distribution of Galaxies  #

This codebase contains R code for computing and visualizing the topology of the distribution of galaxies.

![galaxy-topology.png](https://bitbucket.org/repo/xoedGn/images/2153348347-galaxy-topology.png)

### Running ###

To run the code download the repository and run in R in the R folder:

* install.packages( c("rgl", "misc3d", "RColorBrewer")
* load("elsa-grid100-bw01.Rdata")
* source("rgl.updates.R")
* persistence.plot(1)
* updateContourSelect() 
* click on a point in the persistence diagram to draw the lisosurface o fthe galaxy distribution at that level

### Computing Persistence for New Data Sets###


* install latest version of [mph](https://bitbucket.org/suppechasper/homology)
* Edit setup.rgl.R to load any x,y,z coordinates stored in the varibale X. Edit number of grid points and bandwidth for kernel density estimation. Finally run the script.
