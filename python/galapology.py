import yt
from yt.visualization.volume_rendering.transfer_function_helper import TransferFunctionHelper
from yt.visualization.volume_rendering.api import Scene, Camera, VolumeSource

import numpy as np
import numpy.linalg as la

from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
import matplotlib.pyplot as plt


locs = np.genfromtxt('data/elsa-grid100-bw01-locs.csv', delimiter=' ')
x = np.genfromtxt('data/elsa-grid100-bw01-gridx.csv', delimiter=' ')
y = np.genfromtxt('data/elsa-grid100-bw01-gridy.csv', delimiter=' ')
z = np.genfromtxt('data/elsa-grid100-bw01-gridz.csv', delimiter=' ')
d = np.genfromtxt('data/elsa-grid100-bw01-density.csv', delimiter=' ')
hom = np.genfromtxt('data/elsa-grid100-bw01-hom.csv', delimiter=' ')

d = np.reshape(d, [ x.shape[0], y.shape[0], z.shape[0]])
#d = np.log(1+d)
#d = np.max(d) - d

data = dict(density = 1+d )#, 
           # number_of_particles = locs.shape[0],
           # particle_position_x = (locs[:,0], 'code_length'), 
           # particle_position_y = (locs[:,1], 'code_length'),
           # particle_position_z = (locs[:,2], 'code_length'))

xmin = np.min(x)
xmax = np.max(x)
xd = xmax-xmin
ymin = np.min(y)
ymax = np.max(y)
yd = ymax-ymin
zmin = np.min(z)
zmax = np.max(z)
zd = zmax-zmin

bbox = np.array([ [xmin - 0.001*xd, xmax+0.001*xd],
                  [ymin - 0.001*yd, ymax+0.001*yd], 
                  [zmin - 0.001*zd, zmax+0.001*zd] ] )

ds = yt.load_uniform_grid(data, d.shape, length_unit='cm', bbox=bbox, nprocs=10 )
sc = yt.create_scene(ds)

tf = sc.get_source(0).transfer_function
tf.clear()

tf.add_step(start=0.5, stop=1, value=[1,0,0,100])

#tf.add_step(start=0.3, stop=0.4, value=[0,0,1,10])
#tf.add_layers(3, 0.002, mi=0.2, ma=0.5, col_bounds = [0.2,0.5],
#     alpha=5*np.ones(3,dtype='float64'), colormap = 'RdBu_r')
#tf.grey_opacity = True

cam = sc.camera
cam.resolution = (400, 400)
cam.set_width(ds.domain_width)
#sc.render()
#sc.show(sigma_clip=1)


ad = ds.all_data()
surface = ds.surface(ad, "density", 4.5)

fig = plt.figure()
ax = fig.gca(projection='3d')
p3dc = Poly3DCollection(surface.triangles, linewidth=0.0)
ax.add_collection(p3dc)
max_extent = (surface.vertices.max(axis=1) - surface.vertices.min(axis=1)).max()
centers = (surface.vertices.max(axis=1) + surface.vertices.min(axis=1)) / 2
bounds = np.zeros([3,2])
bounds[:,0] = centers[:] - max_extent/2
bounds[:,1] = centers[:] + max_extent/2
ax.auto_scale_xyz(bounds[0,:], bounds[1,:], bounds[2,:])
plt.show()
